# Commerce One-Click Checkout

Provides a one-click checkout experience for Drupal Commerce.

This feature will only be available to customers who are logged in to accounts with default addresses and stored payment methods.

## Setup

## Configuration
